import subprocess
import os
import fcntl
import psutil
import time
from multiprocessing import cpu_count
from array import array
import sys

_IOC_NRBITS = 8
_IOC_TYPEBITS = 8
_IOC_SIZEBITS = 14
_IOC_DIRBITS = 2

_IOC_NRSHIFT = 0
_IOC_TYPESHIFT = _IOC_NRSHIFT + _IOC_NRBITS
_IOC_SIZESHIFT = _IOC_TYPESHIFT + _IOC_TYPEBITS
_IOC_DIRSHIFT = _IOC_SIZESHIFT + _IOC_SIZEBITS

_IOC_NONE = 0
_IOC_WRITE = 1
_IOC_READ = 2


def _IOC(dir, type, nr, size):
    return dir  << _IOC_DIRSHIFT  | \
           type << _IOC_TYPESHIFT | \
           nr   << _IOC_NRSHIFT   | \
           size << _IOC_SIZESHIFT


def _IO(type, nr): return _IOC(_IOC_NONE, type, nr, 0)
def _IOR(type, nr, size): return _IOC(_IOC_READ, type, nr, size)
def _IOW(type, nr, size): return _IOC(_IOC_WRITE, type, nr, size)
def _IOWR(type, nr, size): return _IOC(_IOC_READ | _IOC_WRITE, type, nr, size)

VQSTATS_QWORDS = 9

VHOST_POLLING_POLLVQ_VIRTIO = 0xc9
VHOST_POLLING_POLLVQ_GET_STATS =   _IOW(VHOST_POLLING_POLLVQ_VIRTIO, 1, VQSTATS_QWORDS * 8)
VHOST_POLLING_POLLVQ_SET_OWNER =   _IO(VHOST_POLLING_POLLVQ_VIRTIO, 2)
VHOST_POLLING_POLLVQ_UNSET_OWNER = _IO(VHOST_POLLING_POLLVQ_VIRTIO, 3)
VHOST_POLLING_POLLVQ_ENABLE_SHARED= _IO(VHOST_POLLING_POLLVQ_VIRTIO, 4)


VHOST_POLLING_POLLWORKER_VIRTIO = 0xc8
VHOST_POLLING_POLLWORKER_ENABLE_SHARED =   _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 1)
VHOST_POLLING_POLLWORKER_RESUME =   _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 2)
VHOST_POLLING_POLLWORKER_SUSPEND =   _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 3)


POLLVQ_INFO_DEVID = 0
POLLVQ_INFO_VQID = 1
POLLVQ_INFO_TSC = 2
POLLVQ_INFO_WROK_COUNT = 3
POLLVQ_INFO_LAST_WORK = 4
POLLVQ_INFO_WORK_TIME = 5
POLLVQ_INFO_STUCK_COUNT = 6
POLLVQ_INFO_LIMITED_COUNT = 7
POLLVQ_INFO_INFO_STUCK_TIME = 8

POLLVQ_INFO_DEVID = 0
POLLVQ_INFO_VQID = 1
POLLVQ_INFO_TSC = 2
POLLVQ_INFO_WROK_COUNT = 3
POLLVQ_INFO_LAST_WORK = 4
POLLVQ_INFO_WORK_TIME = 5
POLLVQ_INFO_STUCK_COUNT = 6
POLLVQ_INFO_LIMITED_COUNT = 7
POLLVQ_INFO_INFO_STUCK_TIME = 8

POLLVQ_INFO_LAST = 9

def get_alive_vq_info(vqs):
    while 1:
        try:
            vqs[os.open('/dev/vhost-polling-vq', os.O_RDONLY)] = { 'worker' : None }
        except:
            break

    remove = []
    out = []
    for vq, data in vqs.items():
        if 'info' in data:
            arr = array("L", VQSTATS_QWORDS * [0]) + data['info'][:POLLVQ_WORDS]
        else:
            arr = array("L", 2 * VQSTATS_QWORDS * [0])
        try:
            fcntl.ioctl(vq, VHOST_POLLING_POLLVQ_GET_STATS, arr, True)
        except:
            remove.append(vq)
            continue
        data['info'] = arr

    for vq in remove:
        vqs.remove(vq)

    return out

iter_count = 0
worker_cpu_usage_sum = 0
vm_cpu_usage_sum = 0

def update_information(vqs, active_workers, inactive_workers):
    global iter_count
    global worker_cpu_usage_sum
    global vm_cpu_usage_sum

    vm_cpu_usage = (psutil.cpu_percent() - 100 * len(active_workers)) / len(inactive_workers)
    if vm_cpu_usage < 10: # idle
        return

    iter_count += 1
    worker_cpu_usage_sum += vm_cpu_usage
    vm_cpu_usage_sum += sum((data['info'][POLLVQ_INFO_WORK_TIME] - data['info'][POLLVQ_INFO_LAST + POLLVQ_INFO]) / 
                             data['info'][POLLVQ_INFO_TSC]           - data['info'][POLLVQ_INFO_LAST + POLLVQ_INFO_TSC] 
                             for vq, data in vqs.items() if vq.owner) / len(active_workers)


def    make_decisions(vqs, active_workers, inactive_workers):
    pass

def dump_information(vqs, vm_count):
    with open('/tmp/manager_statistics', 'wb') as f:
        f.write('Running test on machine with %d CPUs and %s VMs\n' % (cpu_count(), vm_count))
        f.write('Average worker cpu usage: %d\n' % (worker_cpu_usage_sum / iter_count))
        f.write('Average vm cpu usage: %d\n' % (vm_cpu_usage_sum / iter_count))

def daemon(worker_count, vm_count):
    vqs = { }
    active_workers = list()
    inactive_workers = list(os.open('/dev/vhost-polling-worker', os.O_RDONLY) for _ in xrange(cpu_count()))
    if worker_count:
        for _ in xrange(worker_count):
            active_workers.append(inactive_workers[-1])
            fcntl.ioctl(active_workers[-1], VHOST_POLLING_POLLWORKER_ENABLE_SHARED)
            fcntl.ioctl(active_workers[-1], VHOST_POLLING_POLLWORKER_RESUME)
            
            active_workers.pop()
    while True:
        time.sleep(0.5)
        if os.path.exists("/tmp/stop_manager"):
            dump_information(vqs, vm_count, if_names)
            os.unlink("/tmp/stop_manager")
            exit()
        get_alive_vq_info(vqs)
        if not worker_count:
            update_information(vqs, active_workers, inactive_workers)
        else:
            for vq in vqs:
                fcntl.ioctl(vq, VHOST_POLLING_POLLVQ_ENABLE_SHARED)
        make_decisions(vqs, active_workers, inactive_workers)

def main(argv):
    if len(argv) < 2:
        print "Usage: python %s (start|stop)" % __file__
        return 0
    if argv[1] == "start":
        if len(argv) != 4:
            print "Usage: python %s start <worker_count (0 for manager)> <vm_count>" % __file__
            return 0
        if os.fork() > 0:
            os._exit(0)
        os.setsid()
        if os.fork() > 0:
            os._exit(0)
        daemon(int(argv[2]), argv[3])
        os._exit(0)

    elif argv[1] == "stop":
        with open("/tmp/stop_manager", 'wb') as f:
            f.write('')
        while os.path.exists("/tmp/stop_manager"):
            time.sleep(0.2)
        with open("/tmp/manager_statistics", 'rb') as f:
            print f.read()

if __name__ == "__main__":
    os._exit(main(sys.argv))
