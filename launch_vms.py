#!/usr/bin/env python2

import sys
import os
import subprocess as sp
import time

def run_vm(img, tapnum, mac):
    print img.replace("/home/user/benchmark/vmimgs/vm", "").replace(".qcow2", "")
    if not os.fork():
        for i in xrange(3, 1024):
            try:
                os.close(i)
            except OSError:
                pass
        with open('/dev/null', 'r+') as f:
            os.dup2(f.fileno(), 0)
            os.dup2(f.fileno(), 1)
            os.dup2(f.fileno(), 2) 
        with open("/dev/tap" + str(tapnum), "r+") as f:
            general_options = ["--enable-kvm", "-cpu", "qemu64,+x2apic,-kvm_pv_eoi", "-m", "1G", "-hda", img, "-vnc", "0.0.0.0:" + img.replace("/home/user/benchmark/vmimgs/vm", "").replace(".qcow2", "")]
            device_options = ["-netdev", "type=tap,fd=%d,id=guest0,vhost=on" % f.fileno(), "-device", "virtio-net-pci,netdev=guest0,mac=%s" % mac]
            os.setsid()
            os.execv("/usr/bin/qemu-system-x86_64", ["qemu"] + general_options + device_options)

def launch_vms(imgs, tapnums, macs):
    sp.call(['pkill', '-9', 'qemu-system'])
    time.sleep(2)
    for i, t, m in zip(imgs, tapnums, macs):
        run_vm(i, t, m) 

if __name__ == "__main__":
    launch_vms(eval(sys.argv[1]), eval(sys.argv[2]), eval(sys.argv[3]))
