import sys, os
import subprocess as sp
import re
import multiprocessing
import time
import json
import socket

IPS = list("10.10.{0}.{1}".format(i,j) for i in [1,2] for j in xrange(3,9))

def test_ips(ips):
    for ip in ips:
        with open("/dev/null","r+") as f:
            if sp.call(["ping","-c","1",ip],stdin=f,stdout=f,stderr=f):
                return False

    return True

THRESHOLD = 600
SMALL_THRESHOLD = 120

RESTART_COMMAND = "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@{0} env PYTHONPATH=/home/user/benchmark python /home/user/benchmark/prep_for_benchmark.py restart".split(" ")

def prep_for_start_test(host,ips):
    start = inter_start = time.time()
    while time.time() - start < THRESHOLD:
        if test_ips(ips):
            return True
        if inter_start != start and time.time() - inter_start < SMALL_THRESHOLD:
            continue
        with open("/dev/null","r+") as f:
            restart_command = map(lambda x:x.format(host),RESTART_COMMAND)
            print 'Relaunching VMs'
            assert not sp.call(restart_command,stdin=f,stdout=f,stderr=f)
        inter_start = time.time()

    return False

NETPERF_COMMAND = "netperf -C -c -P0 -t TCP_MAERTS -l 60 -D 2 -H {0} -- -m ,{1}".split(" ")

def run_netperf_single_tx_test(host, ips, size, outfile):
	while True:
	    s = socket.socket()
	    print "Preparing... Size: " + str(size)
	    if prep_for_start_test(host, ips):
	        print "Prepared... Starting test."
	    else:
	        print "Fail..."
	        exit(1)
	    s.connect((host, 32768))
	    s.send("Netperf TX Test, Packet Size {0}<<<>>>{1}".format(size, outfile))
	    s.recv(4096)
	    ps = []
	    for ip in ips:
	        netperf_command = map(lambda x:x.format(ip,size),NETPERF_COMMAND)
	        ps.append(sp.Popen(netperf_command, stdin=sp.PIPE, stderr=sp.PIPE, stdout=sp.PIPE))

	    for p in ps:
	    	p.wait()

	    total = 0 
	    for p in ps:
	    	out, _ = p.communicate()
                try:
	    	    tput = float(filter(bool, out.split("\n")[-2].split(" "))[4])
                except ValueError:
		    break
	    	total += tput
            else:
                if test_ips(ips):
                    print "Result: " + str(total)
	            results = { "Total Netperf Throughput" : total }
	            s.send(json.dumps(results))
	            s.close()
	            return
	    s.close()
            print "VM failed during test... restarting"


def run_netperf_tx_test(host, ips, outfile):
	for size in list(2**i for i in xrange (6, 17)):
		run_netperf_single_tx_test(host, ips, size, outfile)

run_netperf_tx_test("tapuz34", IPS, "/root/result.txt")
