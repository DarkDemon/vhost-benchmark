#! /usr/bin/env python2
import sys
import os
import subprocess as sp
import os.path
import shutil
import re

WORKDIR = os.path.dirname(os.path.abspath(__file__))

BASE_IMG = os.path.join(WORKDIR, 'ubuntu.qcow2')
OUT_DIR = os.path.join(WORKDIR, 'vmimgs')
IMG_FMT = 'vm%d.qcow2'
MOUNT_PATH = os.path.join(WORKDIR, 'vmmount')

def create_snapshot(path):
    if sp.call(['qemu-img', 'create', '-f', 'qcow2', '-b', BASE_IMG, path]):
        print "Failed to clone %s to %s" % BASE_IMG, path

def mount_vmimg(path):
    if sp.call(['qemu-nbd', '--connect', '/dev/nbd0', path]):
        print "Failed to connect nbd0 to %s" % path
        return
    if sp.call(['mount', '/dev/nbd0p1', MOUNT_PATH]):
        print "Failed to mount /dev/nbd0p1 to %s" % MOUNT_PATH
        sp.call(['qemu-nbd', '--disconnect', '/dev/nbd0'])
        return

def configure_ip(ip, up_cmd):
    try:
        with open(os.path.join(MOUNT_PATH, 'etc/network/interfaces'), 'r+') as f:
            c = f.read()
            f.seek(0)
            f.truncate()
            content = re.sub(r"iface ([^\s]*) inet dhcp", r"iface \1 inet static\n\taddress %s\n\tnetmask 255.255.255.0\n\tup %s" % (ip, up_cmd), c)
            f.write(content)
    except Exception as e:
        print "Failed to write vm's /etc/netowrk/inerfaces"
    
def umount_vmimg():
    if sp.call(['umount', MOUNT_PATH]):
        print "Failed to unmount %s" % MOUNT_PATH
    if sp.call(['qemu-nbd', '--disconnect', '/dev/nbd0']):
        print "Failed to disconnect /dev/nbd0"

def setup_images(ips, up_cmds):
    shutil.rmtree(OUT_DIR, True)
    os.mkdir(OUT_DIR)
    ret = []
    for idx in xrange(len(ips)):
        path = os.path.join(OUT_DIR, IMG_FMT % idx)  
        ret.append(path)
        create_snapshot(path)
        mount_vmimg(path)
        configure_ip(ips[idx], up_cmds[idx]) 
        umount_vmimg()
    return ret

if __name__ == '__main__':
   print setup_images(['10.10.1.3', '10.10.1.4', '10.10.1.5', '10.10.1.6', '10.10.1.7', '10.10.1.8',
                       '10.10.2.3', '10.10.2.4', '10.10.2.5', '10.10.2.6', '10.10.2.7', '10.10.2.8'], ["true"] * 12)
