#!/bin/bash

for i in $(cat /proc/interrupts | grep i40e-enp.s0f0-TxRx- | awk '{ print $1 }' | sed s'/.$//' | xargs echo)
do
    echo $i
    echo 1 > /proc/irq/$i/smp_affinity
done
