import sys, os
import subprocess as sp
import re
import multiprocessing
import time
import json
import socket

def pre_info_col(ifaces):
    info = { }

    with open('/proc/net/dev', 'rb') as f:
        lines = f.readlines()

    info['ipkts'], info['ibytes'], info['opkts'], info['obytes'] = 0, 0, 0, 0
    for line in lines:
        if "macvtap" not in line:
            continue
        vals = filter(bool, line.split(' '))
        info['ibytes'] += int(vals[1])
        info['ipkts'] += int(vals[2])
        info['obytes'] += int(vals[9])
        info['opkts'] += int(vals[10])


    with open('/proc/interrupts', 'rb') as f:
        lines = f.readlines()

    for iface in ifaces:
        info[iface + '_ints'] = 0

    info['posted_ints'] = 0

    for line in lines:
        for iface in ifaces:
            if iface in line:
                info[iface + '_ints'] += sum(map(int, filter(bool, line.split(' '))[1: multiprocessing.cpu_count() + 1]))

        if 'Posted-interrupt' in line:
            info['posted_ints'] += sum(map(int, filter(bool, line.split(' '))[1: multiprocessing.cpu_count() + 1]))

    info['time'] = time.time()
    return info


def post_info_col(ifaces, pre_info):
    post_info = pre_info_col(ifaces)
    out_info = { }
    for key in post_info:
        out_info[key] = post_info[key] - pre_info[key]

    out_info['itput'] = float(out_info['ibytes']) * 8 / out_info['time'] / 10**9
    out_info['otput'] = float(out_info['obytes']) * 8 / out_info['time'] / 10**9
    out_info['ipktsz'] = float(out_info['ibytes']) / out_info['ipkts']
    out_info['opktsz'] = float(out_info['obytes']) / out_info['opkts']
    return out_info

INFO_NAMES = \
[
    ('time', 'Test Duration (s)'),
    ('opkts', 'Packets Sent'),
    ('obytes', 'Bytes Sent'),
    ('otput', 'Raw TX Throughput (Gbps)'),
    ('opktsz', 'Avg TX Packet Size'),
    ('ipkts', 'Packets Received'),
    ('ibytes', 'Bytes Received'),
    ('itput', 'Raw RX Throughput (Gbps)'),
    ('ipktsz', 'Avg RX Packet Size'),
    ('posted_ints', 'Number of Posted Interrupts')
]

def write_info_file(fname, test_name, results, ifaces, out_info):
    f = open(fname, 'a+b')
    f.write('Result for test:{0}\n'.format(test_name))
    f.write('    Results:\n')
    for key, value in results.items():
        f.write('        {0}:{1}\n'.format(key, value))
    f.write('    Extra Data:\n')
    for key, name in INFO_NAMES:    
        f.write('        {0}:{1}\n'.format(name, out_info[key]))
    for key, value in out_info.items():
        if key.strip('_ints') in ifaces:
            f.write('        Hardware Interrupts for Interface {0}:{1}\n'.format(key.strip('_ints'), value))

def daemonize():
    try:
        pid = os.fork()
        if pid > 0:
            # exit first parent
            sys.exit(0)
    except OSError, e:
        sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    # decouple from parent environment
    os.chdir("/")
    os.setsid()
    os.umask(0)

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # exit from second parent
            sys.exit(0)
    except OSError, e:
        sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    # redirect standard file descriptors
    sys.stdout.flush()
    sys.stderr.flush()
    si = file("/dev/null", 'r')
    so = file("/dev/null", 'r+')
    se = file("/dev/null", 'r+')
    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())

IFACES = ["enp4s0f0","enp5s0f0"]

def main():
    #daemonize()
    s = socket.socket()
    try:
        s.bind(('', 32768))
        s.listen(1)
    except socket.error:
        sys.exit(1)

    while True:
        c, _ = s.accept()
        pre_info = pre_info_col(IFACES)
        test_name, out_file = c.recv(2048).split("<<<>>>")
        c.send("ok")
        try:
            results = json.loads(c.recv(4096))
        except ValueError:
            print "Problem..."
            c.close()
            continue
        out_info = post_info_col(IFACES, pre_info)
        write_info_file(out_file,test_name,results,IFACES,out_info)
        c.send("ok")
        c.close()

if __name__ == '__main__':
    main()
