#!/bin/bash

for i in $(cat /proc/interrupts | grep i40e-enp4s0f0-TxRx- | awk '{ print $1 }' | sed s'/.$//' | xargs echo)
do
    echo $i
    echo 1 > /proc/irq/$i/smp_affinity
done

for i in $(cat /proc/interrupts | grep i40e-enp5s0f0-TxRx- | awk '{ print $1 }' | sed s'/.$//' | xargs echo)
do
    echo $i
    echo 400 > /proc/irq/$i/smp_affinity
done
