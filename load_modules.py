#!/usr/bin/env python2
import subprocess as sp
import os.path

NEEDED_MODULES = ["macvtap", "nbd max_part=32"]
MY_MODULES = ["/home/user/vhost-polling/vhost.ko", "/home/user/vhost-polling/vhost_polling.ko", "/home/user/vhost-polling/vhost_net.ko"]
#MY_MODULES = ["/home/user/vhost-polling-iface/i40e.ko", "/home/user/vhost-polling-iface/vhost.ko", "/home/user/vhost-polling-iface/vhost_polling.ko", "/home/user/vhost-polling-iface/vhost_net.ko"]

def load_modules():
    #if os.path.exists("/tmp/module_time") and os.path.getmtime('/tmp/module_time') > os.path.getmtime(MY_MODULES[0]):
    #    return

    for m in MY_MODULES[::-1] + NEEDED_MODULES[::-1]:
        sp.call(["rmmod", m.split()[0]])
    for m in NEEDED_MODULES:
        sp.call(["modprobe"] + m.split())
    for m in MY_MODULES:
        sp.call(["insmod"] + m.split())

    with open('/tmp/module_time', 'wb') as f:
        f.write('')

if __name__ == '__main__':
    load_modules()
