#!/usr/bin/gnuplot -p
set xlabel "Packet Size"
set ylabel "Throughput (Mb/s)"
set yrange [0:100000]
set style fill solid
set log x
plot "data2.dat" using 1:2:xtic(1) with linespoints lw 2 pt "X" lc rgb "green" title "vhost", \
     "data2.dat" using 1:3:xtic(1) with linespoints lw 2 lt 2 pt "X" lc rgb "blue" title "1 sidecore (irq+napi shared)", \
     "data2.dat" using 1:5:xtic(1) with linespoints lw 2 lt 2 pt "X" lc rgb "red" title "2 sidecores (irq+napi shared)", \
     "data2.dat" using 1:4:xtic(1) with linespoints lw 2 pt "X" lc rgb "cyan" title "1 sidecore (irq+napi bind)", \
     "data2.dat" using 1:6:xtic(1) with linespoints lw 2 pt "X" lc rgb "magenta" title "2 sidecores (irq+napi bind)"