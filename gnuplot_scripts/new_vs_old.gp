#!/usr/bin/gnuplot -p
set title "Throughput over packet size on different GEUST kernels\n12 VMS, host kernel 4.10, TX only, TSO on"
set xlabel "Packet Size"
set ylabel "Throughput (Gb/s)"
set yrange [0:90]
set style fill solid
set log x
plot "data5.dat" using 1:($2/1000):xtic(1) with linespoints lw 2 pt "X" lc rgb "blue" title "3.13", \
     "data5.dat" using 1:($3/1000):xtic(1) with linespoints lw 2 pt "X" lc rgb "red" title "4.10"