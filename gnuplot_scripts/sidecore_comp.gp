#!/usr/bin/gnuplot -p
set xlabel "Packet Size"
set ylabel "Throughput (Mb/s)"
set yrange [0:100000]
set style fill solid
set log x
plot "data4.dat" using 1:2:xtic(1) with linespoints lw 2 pt "X" lc rgb "blue" title "vhost", \
     "data4.dat" using 1:3:xtic(1) with linespoints lw 2 pt "X" lc rgb "red" title "1 sidecore", \
     "data4.dat" using 1:4:xtic(1) with linespoints lw 2 pt "X" lc rgb "green" title "2 sidecores", \
     "data4.dat" using 1:5:xtic(1) with linespoints lw 2 pt "X" lc rgb "orange" title "3 sidecores", \
     "data4.dat" using 1:6:xtic(1) with linespoints lw 2 pt "X" lc rgb "magenta" title "4 sidecores", \
     "data4.dat" using 1:8:xtic(1):(sprintf("x%.02f, %d", $8/$2, $7)) with labels offset 0,1 notitle
