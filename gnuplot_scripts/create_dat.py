import sys

l = open(sys.argv[1]).readlines()
f = open(sys.argv[2], 'w')

for i in xrange (0, len(l), 16):
	f.write("{} {} {} {}\n".format(i / 16 + 1,
		                     int(l[i][len("Result for test:Netperf TX Test, Packet Size "):]),
		                     float(l[i + 2][len("        Total Netperf Throughput:"):]),
		                     float(l[i + 8][len("        Avg TX Packet Size:"):])))

f.close()