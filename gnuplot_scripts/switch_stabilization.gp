#! /usr/bin/gnuplot -p
set terminal svg
set output 'up.svg'

set label 1 'Time (s)' at screen 0.49,0.02
set label 2 'Throughput (Gb/s)' at screen 0.01,0.5 rotate by 90
unset xlabel
unset ylabel
unset tics
set multiplot layout 4, 2 title "Switch stabilization, netperf 2048 (throughput (Gb/s) over time (s))"


set title "0->1"
set xrange [9.5:10.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "1->2"
set xrange [19.5:20.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "2->3"
set xrange [29.5:30.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "3->4"
set xrange [39.5:40.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "4->5"
set xrange [49.5:50.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "5->6"
set xrange [59.5:60.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "6->7"
set xrange [69.5:70.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

unset multiplot

set terminal svg
set output 'down.svg'

set label 1 'Time (s)' at screen 0.49,0.02
set label 2 'Throughput (Gb/s)' at screen 0.01,0.5 rotate by 90
unset xlabel
unset ylabel
unset tics
set multiplot layout 4, 2 title "Switch stabilization, netperf 2048 (throughput (Gb/s) over time (s))"
set title "7->6"
set xrange [79.5:80.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "6->5"
set xrange [89.5:90.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "5->4"
set xrange [99.5:100.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "4->3"
set xrange [109.5:110.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "3->2"
set xrange [119.5:120.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "2->1"
set xrange [129.5:130.5]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

set title "1->0"
set xrange [139:142]
# stats "/tmp/points2048" using 1:($2 * 0.001 * 8) nooutput 
plot "/tmp/points2048" using 1:($2 * 0.001 * 8) with lines lw 2 lc rgb "blue" notitle, "/tmp/breaks2048" using 1:(0):(0):(90) with vectors nohead notitle
unset xrange

unset multiplot 