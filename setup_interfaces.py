#!/usr/bin/python2
import sys
import subprocess as sp
import os

def create_tap(name, mac, backing_dev):
    if sp.call(["ifconfig", backing_dev, "promisc", "up"]):
        print "Failed to raise %s interface" % backing_dev,
        return
    if sp.call(["ip", "link", "add", "link", backing_dev, "name", name, "type", "macvtap"]):
        print "Failed to create %s with mac %s on %s" % (name, mac, backing_dev)
        return
    if sp.call(["ip", "link", "set", name, "address", mac, "up"]):
        print "Failed to rise %s" % name
    ip_a, _ = sp.Popen(["ip", "address"], stdout=sp.PIPE, stderr=sp.PIPE).communicate()
    line = [x for x in ip_a.splitlines() if name + "@" + backing_dev + ":" in x][0]
    return int(line[:line.index(":")])

def setup_interfaces(names, macs, backing_devs):
    return [create_tap(n, m, b) for n, m, b in zip(names, macs, backing_devs)]

if __name__ == '__main__':
    macs = ["aa:aa:aa:aa:aa:" + str(i).zfill(2) for i in xrange(12)]
    print macs
    print setup_interfaces(["macvtap" + str(i) for i in xrange(12)], 
                           macs,
                           ["enp5s0f0"] * 6 + ["enp5s0f1"] * 6)
