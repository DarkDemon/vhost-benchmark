#!/usr/bin/env python2
import os
import os.path
import sys
import subprocess as sp

def disable_numa_hyperthreading():
    out = sp.check_output(['/usr/bin/lscpu'])
    if "Thread(s) per core:    1" in out and "Socket(s):             1" in out:
        return
    path, dirs, _ = os.walk('/sys/devices/system/cpu').next()
    phys = set()
    with open('/sys/devices/system/cpu/cpu0/topology/core_id') as f:
        phys.add(f.read())
    dirs.sort()
    for dirname in dirs:
        if dirname.startswith('cpu'):
            try:
                with open(os.path.join(path, dirname, 'online'), 'wb') as f:
                    f.write('1')
                with open(os.path.join(path, dirname, 'topology/core_id')) as f:
                    core_id = f.read()
            except IOError:
                continue
            _, dirs2, _ = os.walk(os.path.join(path, dirname)).next()
            if core_id in phys or any([x.startswith('node') and x != 'node0' for x in dirs2]):
                with open(os.path.join(path, dirname, 'online'), 'wb') as f:
                    f.write('0')
            else:
                phys.add(core_id)

if __name__ == '__main__':
    disable_numa_hyperthreading()
