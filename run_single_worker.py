#!/usr/bin/env python2
import os, fcntl
from array import array

import psutil
import time
import sys
import math

CPU_COUNT = 8

vqs = []

while 1:
    try:
        vqs.append(os.open('/dev/vhost-polling-vq', os.O_RDONLY))
    except:
        break

_IOC_NRBITS = 8
_IOC_TYPEBITS = 8
_IOC_SIZEBITS = 14
_IOC_DIRBITS = 2

_IOC_NRSHIFT = 0
_IOC_TYPESHIFT = _IOC_NRSHIFT + _IOC_NRBITS
_IOC_SIZESHIFT = _IOC_TYPESHIFT + _IOC_TYPEBITS
_IOC_DIRSHIFT = _IOC_SIZESHIFT + _IOC_SIZEBITS

_IOC_NONE = 0
_IOC_WRITE = 1
_IOC_READ = 2


def _IOC(dir, type, nr, size):
    return dir  << _IOC_DIRSHIFT  | \
           type << _IOC_TYPESHIFT | \
           nr   << _IOC_NRSHIFT   | \
           size << _IOC_SIZESHIFT


def _IO(type, nr): return _IOC(_IOC_NONE, type, nr, 0)
def _IOR(type, nr, size): return _IOC(_IOC_READ, type, nr, size)
def _IOW(type, nr, size): return _IOC(_IOC_WRITE, type, nr, size)
def _IOWR(type, nr, size): return _IOC(_IOC_READ | _IOC_WRITE, type, nr, size)

VQSTATS_QWORDS = 10

VHOST_POLLING_POLLVQ_VIRTIO = 0xc9
VHOST_POLLING_POLLVQ_GET_STATS =   _IOW(VHOST_POLLING_POLLVQ_VIRTIO, 1, VQSTATS_QWORDS * 8)
VHOST_POLLING_POLLVQ_SET_OWNER =   _IO(VHOST_POLLING_POLLVQ_VIRTIO, 2)
VHOST_POLLING_POLLVQ_UNSET_OWNER = _IO(VHOST_POLLING_POLLVQ_VIRTIO, 3)
VHOST_POLLING_POLLVQ_ENABLE_SHARED= _IO(VHOST_POLLING_POLLVQ_VIRTIO, 4)


VHOST_POLLING_POLLWORKER_VIRTIO = 0xc8
VHOST_POLLING_POLLWORKER_ENABLE_SHARED =   _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 1)
VHOST_POLLING_POLLWORKER_RESUME =   _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 2)
VHOST_POLLING_POLLWORKER_SUSPEND =   _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 3)


POLLVQ_INFO_DEVID = 0
POLLVQ_INFO_VQID = 1
POLLVQ_INFO_TSC = 2
POLLVQ_INFO_WROK_COUNT = 3
POLLVQ_INFO_LAST_WORK = 4
POLLVQ_INFO_WORK_TIME = 5
POLLVQ_INFO_STUCK_COUNT = 6
POLLVQ_INFO_LIMITED_COUNT = 7
POLLVQ_INFO_STUCK_TIME = 8
POLLVQ_INFO_BYTES_SR = 9

dummy = []

for i in xrange(8):
    w = os.open('/dev/vhost-polling-worker', os.O_RDONLY) 
    fcntl.ioctl(w, VHOST_POLLING_POLLWORKER_ENABLE_SHARED)
    dummy.append(w)

workers = []

def add_worker():
	workers.append(dummy.pop())
	fcntl.ioctl(workers[-1], VHOST_POLLING_POLLWORKER_RESUME) 

def remove_worker():
	dummy.append(workers.pop())
	fcntl.ioctl(dummy[-1], VHOST_POLLING_POLLWORKER_SUSPEND) 

bufs = [(array("L", VQSTATS_QWORDS * [0]), 0) for _ in xrange(len(vqs))]
p = open('/dev/shm/points', 'wb')
r = open('/dev/shm/breaks', 'wb')
j = 0
up = True
overhead = 0
start = time.time()
collect = False
try:
	while True:
		if overhead > 0.01:
			print "Overhead too large: " + str(overhead)
		else:
			time.sleep(0.01 - overhead)
		t = time.time()
		if math.floor((time.time() - start) / 10) > j:
			if up:
				if len(workers) == 0:
					print "Adding first worker"
					for vq in vqs:
					   fcntl.ioctl(vq, VHOST_POLLING_POLLVQ_ENABLE_SHARED)
					add_worker()
				else:
					print "Adding worker"
					add_worker()
					if len(workers) == 7:
						up = False
			else:
				if len(workers) == 0:
					print "Done"
					raise KeyboardInterrupt
				elif len(workers) == 1:
					print "Removing last worker"
					remove_worker()
					for vq in vqs:
					   fcntl.ioctl(vq, VHOST_POLLING_POLLVQ_UNSET_OWNER)
				else:
					print "Removing worker"
					remove_worker()
			r.write("%f\n" % (time.time() - start, ))
			j += 1

		throughput = 0

		a = bufs[0][1]
		for i, vq in enumerate(vqs):
			old_buf, old_time = bufs[i]
			new_buf, new_time  = array("L", VQSTATS_QWORDS * [0]), time.time()
			fcntl.ioctl(vq, VHOST_POLLING_POLLVQ_GET_STATS, new_buf)
			throughput += float(new_buf[POLLVQ_INFO_BYTES_SR] - old_buf[POLLVQ_INFO_BYTES_SR]) / (new_time - old_time)
			bufs[i] = new_buf, new_time

		if collect:
			p.write("%f %f %f %f\n" % (time.time() - start, throughput / 1000000.0, a, bufs[0][1]))
		else:
			collect = True

		overhead = time.time() - t
except KeyboardInterrupt:
	r.close()
	p.close()