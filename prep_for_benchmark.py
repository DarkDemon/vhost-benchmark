#! /usr/bin/env python2

import sys
import os
from disable_numa_hyperthreading import disable_numa_hyperthreading
from setup_interfaces import setup_interfaces
from setup_images import setup_images
from launch_vms import launch_vms
from load_modules import load_modules
import time

def main(argv):
    if len(argv) < 2 or argv[1] not in ["start", "restart"]:
        print "Usage: %s <start/restart>" % __file__
        return 1

    if argv[1] == "start":
        if len(argv) != 3:
            print "Usage: %s start <config file>" % __file__
            return 1
        
        with open(argv[2], 'rb') as config:
            conf = config.read()

        ips = [x.split()[0] for x in conf.splitlines()]
        macs = [x.split()[1] for x in conf.splitlines()]
        backing_devs = [x.split()[2] for x in conf.splitlines()]
        names = [x.split()[3] for x in conf.splitlines()]
        up_cmds = [x[x.index("::") + 2:] for x in conf.splitlines()]  
        disable_numa_hyperthreading()
        load_modules()
        imgs = setup_images(ips, up_cmds)
        tapnums = setup_interfaces(names, macs, backing_devs)
        with open('/tmp/imgs', 'wb') as f:
            f.write(repr(imgs))
        with open('/tmp/tapnums', 'wb') as f:
            f.write(repr(tapnums))
        with open('/tmp/macs', 'wb') as f:
            f.write(repr(macs))
    else:
        try:
            with open('/tmp/imgs') as f:
                imgs = eval(f.read())
            with open('/tmp/tapnums') as f:
                tapnums = eval(f.read())
            with open('/tmp/macs') as f:
                macs = eval(f.read())
        except IOError:
            print "No imgs or tapnums or macs file in /tmp"
            return 1

    launch_vms(imgs, tapnums, macs)
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

